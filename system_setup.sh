#!/bin/bash

# At the moment, this does not handle the full machine setup,
# but this does take care of the package requirements.
# You will likely need to do: chmod +x ./system_setup.sh before this will run

# usage: ./system_setup.sh -o linux -- or -- ./system_setup -o osx

function usage
{
    echo "usage: ./system_setup.sh [-o operating_system ] | [-h]"
}

while [ "$1" != "" ]; do
    case $1 in
        -o | --operating_system )     shift
                                			operating_system=$1
                                			;;
        -h | --help )           			usage
                                			exit
                                			;;
        * )                     			usage
                                			exit 1
    esac
    shift
done

. venv/bin/activate

if [[ "$operating_system" = "linux" ]]; then

	sudo apt-get update
	sudo apt-get upgrade
	sudo apt-get install libmysqlclient-dev mysql-server mysql-client
	sudo apt-get install libtiff4-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.5-dev tk8.5-dev python-tk

elif [[ "$operating_system" = "osx" ]]; then

	brew up
	brew install libtiff libjpeg webp littlecms

fi

# Why the fuck is this so long?
# See http://kaspermunck.github.io/2014/03/fixing-clang-error/
ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future pip install -r requirements.txt
