. venv/bin/activate

Restart NGINX to save config changes:
sudo service nginx restart

Stop/Start NGINX:
sudo service nginx stop
sudo service nginx start

run uwsgi in the foreground (inside venv - should it not be inside venv?)
uwsgi --socket 127.0.0.1:8080 -w manage:app

run uwsgi in the background (inside venv)
uwsgi --socket 127.0.0.1:8080 -w manage:app --logto /var/log/uwsgi/log.log &

uwsgi --ini /root/bitfluence/uwsgi_config.ini

edit nginx config file:
nano /etc/nginx/nginx.conf

mysql root pass:
blinkr44#


To Choose Tier
================

echo "FLASK_CONFIG=<environment>" > .env

where environment is one of 'development', 'testing', or 'production'


---------------------------------------------

To stop the server
==================

sudo service nginx stop
uwsgi --stop /tmp/bitfluence_pid.pid

To start the server
===================

sudo service nginx start
# only works in venv, since uwsgi is not installed outside of it...
uwsgi --ini /root/bitfluence/uwsgi_config.ini

To reload the server
====================

sudo service nginx restart
uwsgi --reload /tmp/bitfluence_pid.pid
