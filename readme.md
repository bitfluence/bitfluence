Common Commands
========

    python manage.py runserver
    python manage.py shell

To create (initial) and activate the virtual enviornment:

	virtualenv venv
    . venv/bin/activate

To install all dependencies (if you have pip):

	(for linux): sudo apt-get install libmysqlclient-dev
	pip install -r requirements.txt

If you have python-mysql install issues:

  export PATH=$PATH:/usr/local/mysql/bin
  (or to wherever your mysql/bin is)
  then pip install

To create the database

	python manage.py db upgrade

To run the email server in development:

    python -m smtpd -n -c DebuggingServer localhost:1025

Very basic search functionality works.
