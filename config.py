import os
basedir = os.path.abspath(os.path.dirname(__file__))

#Flask-WTF configuration
CSRF_ENABLED = True
#SECRET_KEY = 'bitcoin-creates-a-new-world-order'

class Config: #Contains settings common to all configurations.
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'alt-bitcoin-creates-a-new-world-order'
    AES_KEY = '0f9d8a35d6d4bea5'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    APP_MAIL_SUBJECT_PREFIX = '[Bitfluence]'
    APP_MAIL_SENDER = 'Bitfluence Admin <admin@bitfluence.com>'
    BITFLUENCE_ADMINS = [ 'botellde@gmail.com' ]
    MAX_SEARCH_RESULTS = 50
    TWITTER_CONSUMER_KEY = 'ZFGVWqNJVUNCJMoYVxhkQ'
    TWITTER_CONSUMER_SECRET = 'FEYhe1Jp67txiNzloovBX2qq83cs6X81DpaJ6Awcju0'
    @staticmethod
    def init_app(app):
        pass
        #can add initialization specific configuration later

class DevelopmentConfig(Config):

    DEBUG = True
    MAIL_SERVER = 'smtp.mandrillapp.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = 'botellde@gmail.com'
    MAIL_PASSWORD = 'K53zBF_iVBg6izaIyW6eqg'
    MYSQL_USERNAME = 'bitfluence'
    MYSQL_PASSWORD = 'SuGqwDQ8JyiD'
    MYSQL_DATABASE = 'bitfluence'

    URL = None

    COINBASE_CONSUMER_KEY = '7e3b3777dab3fae7f3eed8ea4e59c4f27c01cfacdc2821843b88043f652dd18f'
    COINBASE_CONSUMER_SECRET = '3db20f247adae40098061bcf8e44d15099f605d356f2fcd9e8e982879d7a12d2'

    SQLALCHEMY_DATABASE_URI = 'mysql://%s:%s@localhost/%s' % (MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE)


    # Settings for uploaded photos
    UPLOADED_PHOTOS_DEST = os.path.join(basedir, "app/static/uploads/photos/")
    #The URL the photos are publicly accessable from.
    UPLOADED_PHOTOS_URL = '127.0.0.1:5000/'


#class TestingConfig(Config):
#   TESTING = True
#   SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
#   'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')


class TestingConfig(Config):
    
    DEBUG = True
    MAIL_SERVER = 'smtp.mandrillapp.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = 'botellde@gmail.com'
    MAIL_PASSWORD = 'K53zBF_iVBg6izaIyW6eqg'
    MYSQL_USERNAME = 'bitfluence'
    MYSQL_PASSWORD = 'SuGqwDQ8JyiD'
    MYSQL_DATABASE = 'bitfluence'

    URL = "http://107.170.26.44/"

    COINBASE_CONSUMER_KEY = 'd465306d7bc6cc40504d6571e12662c390debdc6caa2149efc34b3cd7cfc6664'
    COINBASE_CONSUMER_SECRET = '3fd57de6db7ca26700406b0709d02ac49735e9b2ac7f23ede1b96ea576b3c3a2'

    SQLALCHEMY_DATABASE_URI = 'mysql://%s:%s@localhost/%s' % (MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE)

    # Settings for uploaded photos
    UPLOADED_PHOTOS_DEST = os.path.join(basedir, "app/static/uploads/photos/")
    #The URL the photos are publicly accessable from.
    UPLOADED_PHOTOS_URL = 'http://107.170.26.44/'

class ProductionConfig(Config):

    DEBUG = False
    # Mail is sent via Mandrill, replies are sent to Gmail box.
    MAIL_SERVER = 'smtp.mandrillapp.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = 'botellde@gmail.com'
    MAIL_PASSWORD = 'K53zBF_iVBg6izaIyW6eqg'
    MYSQL_USERNAME = 'bitfluence'
    MYSQL_PASSWORD = 'SuGqwDQ8JyiD'
    MYSQL_DATABASE = 'bitfluence'

    URL = "https://Bitfluence.com/"

    COINBASE_CONSUMER_KEY = 'd3dcc1fca1771f445ee73d65846f7891d39418a8c0078a15c69627136cc2e237'
    COINBASE_CONSUMER_SECRET = '96629939f807f7d4ada63a36fc5a8a7b4cf18d0e9fe3173719049c320e0f46e7'

    SQLALCHEMY_DATABASE_URI = 'mysql://%s:%s@localhost/%s' % (MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE)

    # Settings for uploaded photos
    UPLOADED_PHOTOS_DEST = os.path.join(basedir, "app/static/uploads/photos/")
    #The URL the photos are publicly accessable from.
    UPLOADED_PHOTOS_URL = 'https://bitfluence.com/'



config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
}



