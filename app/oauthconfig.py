from app import oauth


twitter = oauth.remote_app('twitter',
    base_url='https://api.twitter.com/1.1/',
    request_token_url='https://api.twitter.com/oauth/request_token',
    access_token_url='https://api.twitter.com/oauth/access_token',
    authorize_url='https://api.twitter.com/oauth/authenticate',
    app_key='TWITTER'
)
coinbase = oauth.remote_app('coinbase',
    request_token_params={'scope': 'merchant'},
    base_url='https://coinbase.com/api/v1/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://coinbase.com/oauth/token',
    authorize_url='https://coinbase.com/oauth/authorize?r=52807b8ea917e47f32000079&utm_campaign=user-referral&src=referral-link',
    app_key='COINBASE'
)
