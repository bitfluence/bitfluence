Hi {{ user.username }},

You just received {{ payment.amount_mbtc() }} mBTC ({{ "%.2f"|format(payment.amount_native_formatted()) }} {{ payment.native_currency }}) from
{% if payment.payer != None %}
@{{ payment.payer.username }}
{% else %}
anonymous
{% endif %}
{% if payment.message_text != None %}
, with the message:
{{ payment.message_text }}
{% else %}
.
{% endif %}

{{ goodbye }}

- The Bitfluence Team
