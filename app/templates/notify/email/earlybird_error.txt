Hi {{ user.username }},

We have an exciting update we would love to share with you: Bitfluence now includes a built-in receipt system! When you send money through Bitfluence, you can attach a private message to the payment (e.g. “Here’s the money for July rent, you big dinkus!”) that only you and the receiver will be able to read.

However...because of a technical goof on our part, we need you to re-add your Coinbase wallet to your Bitfluence account before this feature will work for money sent to you. (It will already work for money you send.) To avoid confusion for other users, we have already removed your Coinbase wallet from your Bitfluence account.

ALSO As our way of saying thank you (and also showing off our cool stuff!), we will be sending $1 to each and every one of our earliest users (that’s you!) once we publicly launch this feature. So re-add that wallet, try out the new Bitfluence, and let us know what you think!

Have questions? I’ll answer them.
