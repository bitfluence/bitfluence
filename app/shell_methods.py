from models import User
from oauthconfig import twitter
import os
import requests
from StringIO import StringIO
from PIL import Image
from email import send_earlybird_error_email
from . import db

def resize_and_crop(img_path, modified_path, size, crop_type='top'):
    """
    Resize and crop an image to fit the specified size.

    args:
        img_path: path for the image to resize.
        modified_path: path to store the modified image.
        size: `(width, height)` tuple.
        crop_type: can be 'top', 'middle' or 'bottom', depending on this
            value, the image will cropped getting the 'top/left', 'midle' or
            'bottom/rigth' of the image to fit the size.
    raises:
        Exception: if can not open the file in img_path of there is problems
            to save the image.
        ValueError: if an invalid `crop_type` is provided.
    """
    # If height is higher we resize vertically, if not we resize horizontally
    img = Image.open(img_path)
    # Get current and desired ratio for the images
    img_ratio = img.size[0] / float(img.size[1])
    ratio = size[0] / float(size[1])
    #The image is scaled/cropped vertically or horizontally depending on the ratio
    if ratio > img_ratio:
        img = img.resize((size[0], size[0] * img.size[1] / img.size[0]),
                Image.ANTIALIAS)
        # Crop in the top, middle or bottom
        if crop_type == 'top':
            box = (0, 0, img.size[0], size[1])
        elif crop_type == 'middle':
            box = (0, (img.size[1] - size[1]) / 2, img.size[0], (img.size[1] + size[1]) / 2)
        elif crop_type == 'bottom':
            box = (0, img.size[1] - size[1], img.size[0], img.size[1])
        else :
            raise ValueError('ERROR: invalid value for crop_type')
        img = img.crop(box)
    elif ratio < img_ratio:
        img = img.resize((size[1] * img.size[0] / img.size[1], size[1]),
                Image.ANTIALIAS)
        # Crop in the top, middle or bottom
        if crop_type == 'top':
            box = (0, 0, size[0], img.size[1])
        elif crop_type == 'middle':
            box = ((img.size[0] - size[0]) / 2, 0, (img.size[0] + size[0]) / 2, img.size[1])
        elif crop_type == 'bottom':
            box = (img.size[0] - size[0], 0, img.size[0], img.size[1])
        else :
            raise ValueError('ERROR: invalid value for crop_type')
        img = img.crop(box)
    else :
        img = img.resize((size[0], size[1]),
                Image.ANTIALIAS)
        # If the scale is the same, we do not need to crop
    img.save(modified_path)

def get_twitter_images():
    no_id = 0
    existing = 0
    new_pic = 0
    failsave = 0
    failrequest = 0
    users = User.query.all()
    for u in users:
        if u.twitter_id == None:
            no_id += 1
            print 'No Twitter ID was found for <%s>' %(u.username)
        elif u.picture[1:7] == 'static':
            existing += 1
            print 'Local image path found for user <%s>' %(u.username)
        else:
            resp = twitter.request('users/show.json?user_id=%s' % u.twitter_id, method='GET', tokens=(u.oauth_token, u.oauth_secret))
            if resp.status == 200:
                twit_pic = resp.data['profile_image_url_https']
                twit_pic_normalsize = twit_pic.replace("_normal","")
                app_dir = os.path.dirname(os.path.abspath(models.__file__))
                photos_dir = os.path.join(app_dir, "static/uploads/photos/")

                filename = twit_pic_normalsize.split('/')[-1]
                profile_img_location = photos_dir + filename
                cropped_filename = 'cropped_' + filename
                cropped_profile_img_location = photos_dir + cropped_filename

                r = requests.get(twit_pic_normalsize, verify=False)
                if r.status_code == 200:
                    i = Image.open(StringIO(r.content))
                    i.save(profile_img_location)
                    print 'Picture saved locally for user <%s>' %(u.username)
                    new_pic += 1
                else:
                    print 'Failed to get the picture from Twitter for user <%s>' %(u.username)
                    failsave += 1

                resize_and_crop(profile_img_location, cropped_profile_img_location, (200,200), crop_type='middle')
                u.picture = '/static/uploads/photos/' + cropped_filename
                db.session.commit()
            else:
                print 'Failed twitter request for user <%s>' %(u.username)
                failrequest += 1

    return "DONE with %s profiles that dont have an ID, %s existing pictures, %s new pics saved, and %s failed requests to twitter, and %s images failed to save." %(no_id, existing, new_pic, failrequest, failsave)

def email_earlybird_error_people():
    users_to_reset = User.query.filter(User.coinbase_code != None).filter(User.coinbase_oauth_token == None).all()
    users_to_email = User.query.filter(User.coinbase_code != None).filter(User.coinbase_oauth_token == None).filter(User.coinbase_email != None).all()

    for u in users_to_reset:
        u.coinbase_code = None
        db.session.add(u)
    db.session.commit()

    for u in users_to_email:
        send_earlybird_error_email(u)

