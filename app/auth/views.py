from flask import render_template, redirect, request, url_for, flash, g, jsonify, json
from flask.ext.login import login_user, logout_user, login_required, current_user
from . import auth
from .. import db
from .. import models
from ..models import User
from .forms import LoginForm, RegistrationForm, PasswordResetRequestForm, PasswordResetForm, \
    ChangePasswordForm, ChangeEmailForm, ObtainEmailForm
from ..email import send_async_email, send_email
from ..oauthconfig import twitter
import os
import urllib, uuid
from ..photo_methods import resize_and_crop
import requests
from StringIO import StringIO
from PIL import Image


"""
@auth.before_request
def before_request():
    if current_user.is_authenticated():
        current_user.ping()
        if not current_user.confirmed:
            return redirect(url_for('confirm.unconfirmed'))
@auth.before_request
def before_request():
    if current_user.is_authenticated():
        current_user.ping()
        if current_user.email is None:
            return redirect(url_for('confirm.unconfirmed'))
"""

@twitter.tokengetter
def get_twitter_token():
    if current_user.is_authenticated():
        return (current_user.oauth_token, current_user.oauth_secret)
    else:
        return None

@auth.route('/logout')
def logout():
    if current_user.is_authenticated():
        logout_user()
    return redirect(url_for('main.index'))

@auth.route('/login')
def login_twitter():
    if current_user.is_authenticated():
        return redirect(url_for('main.user', username=current_user.username))
    callback_url = url_for('auth.oauth_authorized', next=request.args.get('next'), _external=True)
    return twitter.authorize(callback=callback_url)

@auth.route('/oauth-authorized')
@twitter.authorized_handler
def oauth_authorized(resp):
    next_url = url_for('main.index')
    if resp is None:
        return redirect(next_url)
    twitter_id_resp = resp['user_id']
    twitter_user = User.query.filter_by(twitter_id=twitter_id_resp).first()

    if twitter_user is None or twitter_user.username is None:
        new_twitter_user = User(twitter_id=twitter_id_resp, oauth_token=resp['oauth_token'], oauth_secret=resp['oauth_token_secret'])
        db.session.add(new_twitter_user)
        db.session.commit()
        login_user(new_twitter_user)
        twitter_resp = twitter.request('account/verify_credentials.json', method='GET')
        if twitter_resp.status == 200:
            """
            This code is responsible for getting the twitter picture url, saving the original image,
            cropping the image and saving the file with a 'cropped_' prefix, and saving the publicly
            accessable URL in the user model. This code only runs on the initial login.
            The absolute image save path is determined based on the location of models.py.
            Primarily depends on: urllib, Pillow, and libjpeg (for jpg support).
            """
            twit_pic = twitter_resp.data['profile_image_url_https']
            twit_pic_normalsize = twit_pic.replace("_normal","")
            app_dir = os.path.dirname(os.path.abspath(models.__file__))
            photos_dir = os.path.join(app_dir, "static/uploads/photos/")

            filename = twit_pic_normalsize.split('/')[-1]
            profile_img_location = photos_dir + filename
            cropped_filename = 'cropped_' + filename
            cropped_profile_img_location = photos_dir + cropped_filename

            #resource = urllib.urlretrieve(twit_pic_normalsize, profile_img_location)
            r = requests.get(twit_pic_normalsize, verify=False)
            if r.status_code == 200:
                i = Image.open(StringIO(r.content))
                i.save(profile_img_location)
            else:
                return 'Failed to get the picture from Twitter for user <%s>' %(current_user.username)
            resize_and_crop(profile_img_location, cropped_profile_img_location, (200,200), crop_type='middle')
            current_user.picture = '/static/uploads/photos/' + cropped_filename

            """
            End photo code.
            """
            current_user.username = twitter_resp.data['screen_name']
            current_user.name = twitter_resp.data['name']
            current_user.location = twitter_resp.data['location']
            current_user.about_me = twitter_resp.data['description']
            verified_status = str(twitter_resp.data['verified'])
            if verified_status == 'True':
                current_user.verified = True
            db.session.commit()
            return redirect(url_for('main.user', username=current_user.username))
        else:
            # Make a template for this.
            return "There was an error loading your Twitter profile."

    # We update the authenciation token in the db. In case the user temporarily revoked access, we have new tokens here.
    twitter_user.oauth_token = resp['oauth_token']
    twitter_user.oauth_secret = resp['oauth_token_secret']
    login_user(twitter_user)
    twitter_resp = twitter.request('account/verify_credentials.json', method='GET')
    if twitter_resp.status == 200:
        current_user.username = twitter_resp.data['screen_name']
        verified_status = str(twitter_resp.data['verified'])
        if verified_status == 'True':
            current_user.verified = True
        db.session.commit()
    return redirect(url_for('main.user', username=current_user.username))

#This code is not required.
"""

@auth.route('/active-tokens')
@login_required
def get_id_num():
    # This is a test change.
    active = 0
    disabled = 0
    users = User.query.all()
    for u in users:
        resp = twitter.request('https://api.twitter.com/1.1/users/show.json?user_id=%s' % u.twitter_id, method='GET')
        if resp.status == 200:
            active += 1
        else:
            disabled += 1

    return 'DONE with %s active tokens, %s disabled tokens' %(active, disabled)

@auth.route('/twitter-id')
@login_required
def get_id():
    already_pop = 0
    newly_pop = 0
    no_resp = 0

    users = User.query.all()
    for u in users:
        if u.twitter_id == None:
            resp = twitter.request('https://api.twitter.com/1.1/users/show.json?screen_name=%s' % u.username, method='GET')
            if resp.status == 200:
                twitter_id_resp = resp.data['id_str']
                u.twitter_id = twitter_id_resp
                db.session.commit()
                newly_pop += 1
            else:
                no_resp += 1
        else:
            already_pop += 1
    return 'DONE with %s already populated, %s newly populated, and %s errors with receiving a value.' %(already_pop, newly_pop, no_resp)

@auth.route('/twitter-test')
@login_required
def get_picture():
    #this is a playground
    twit_pic_resp = twitter.request('account/verify_credentials.json', method='GET')
    if twit_pic_resp.status == 200:
        verified_status = str(twit_pic_resp.data['verified'])
        if verified_status == 'True':
            current_user.verified = True
            db.session.commit()
            return "You are verified and have been saved as such."
    return redirect(url_for('main.user', username=current_user.username))

@auth.route('/twitter-test')
@login_required
def get_picture():
    #this is a playground
    twit_pic_resp = twitter.request('account/verify_credentials.json', method='GET')
    printthis = twit_pic_resp.data['profile_image_url_https']
    printthis2 = printthis.replace("_normal","")
    #if twit_pic_resp.status == 200:
        #twit_pic = twit_pic_resp.data['profile_image_url_https']
        #current_user.picture = str(twit_pic)
        #db.session.commit()
    return printthis2

@auth.route('/unconfirmed')
def unconfirmed():
    form = ObtainEmailForm()
    if current_user.is_anonymous() or current_user.confirmed:
        return redirect('main.index')
    if form.validate_on_submit():
        current_user.email = form.email.data
        db.session.commit()
        token = current_ser.generate_confirmation_token()
        send_async_email(current_user.email, 'Confirm Your Account', 'auth/email/confirm', user=user, token=token)
        flash('A confirmation email has been sent to you by email.')
        return redirect(url_for('main.index'))
    return render_template('auth/unconfirmed.html', form=form)

@auth.route('/confirm/<token>')
@login_required
def confirm(token):
    if current_user.confirmed: #is the user already confirmed?
        return redirect(url_for('main.index'))
    if current_user.confirm(token):
        flash('You have confirmed your account. Thanks!')
    else:
        flash('The confirmation link is invalid or has expired.')
    return redirect(url_for('main.index'))

@auth.route('/confirm')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_email('auth/email/confirm', 'Confirm Your Account', user, token=token)
    flash('A new confirmation email has been sent to you by email.')
    return redirect(url_for('main.index'))

@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(email=form.email.data, username=form.username.data, password=form.password.data)
        db.session.add(user)
        db.session.commit() #Required so user is assigned an ID upon commit, and ID can be included in confirmation token.
        token = user.generate_confirmation_token()
        send_email(user.email, 'Confirm Your Account', 'auth/email/confirm', user=user, token=token)
        flash('A confirmation email has been sent to you by email.')
        return redirect(url_for('main.index'))
    return render_template('auth/register.html', form=form)

@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm() #instance of login form.
    if form.validate_on_submit(): #if POST request
        user = User.query.filter_by(email=form.email.data).first() #create user object
        if user is not None and user.verify_password(form.password.data): #if username and password are right
            login_user(user) #then login the user
            return redirect(request.args.get('next') or url_for('main.user', username=current_user.username))
        flash('Invalid username or password.')
    return render_template('auth/login.html', form=form)

@auth.route('/reset', methods=['GET', 'POST'])
def password_reset_request():
    if not current_user.is_anonymous():
        return redirect(url_for('main.index'))
    form = PasswordResetRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            token = user.generate_reset_token()
            send_email(user.email, 'Reset Your Password',
                       'auth/email/reset_password',
                       user=current_user, token=token,
                       next=request.args.get('next'))
        flash('An email with instructions to reset your password has been sent to you.')
        return redirect(url_for('auth.login'))
    return render_template('auth/reset_password_request.html', form=form)

#After password email is sent, user clicks on email link to generate this view.
@auth.route('/reset/<token>', methods=['GET', 'POST'])
def password_reset(token):
    if not current_user.is_anonymous():
        return redirect(url_for('main.index'))
    form = PasswordResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None:
            return redirect(url_for('main.index'))
        if user.reset_password(token, form.password.data):
            flash('Your password has been updated.')
            return redirect(url_for('auth.login'))
        else:
            return redirect(url_for('main.index'))
    return render_template('auth/reset_password.html', form=form)

@auth.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.password.data
            db.session.add(current_user)
            flash('Your password has been updated.')
            return redirect(url_for('main.index'))
        else:
            flash('Invalid password.')
    return render_template("auth/change_password.html", form=form)



@auth.route('/change-email', methods=['GET', 'POST'])
@login_required
def change_email_request():
    form = ChangeEmailForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            new_email = form.email.data
            token = current_user.generate_email_change_token(new_email)
            send_email(new_email, 'Confirm your email address',
                       'auth/email/change_email',
                       user=current_user, token=token)
            flash('An email with instructions to confirm your new email '
                  'address has been sent to you.')
            return redirect(url_for('main.index'))
        else:
            flash('Invalid email or password.')
    return render_template("auth/change_email.html", form=form)


@auth.route('/change-email/<token>')
@login_required
def change_email(token):
    if current_user.change_email(token):
        flash('Your email address has been updated.')
    else:
        flash('Invalid request.')
    return redirect(url_for('main.index'))
"""
