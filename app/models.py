from datetime import datetime
from . import db, login_manager
from passlib.apps import custom_app_context as pwd_context
from flask.ext.login import UserMixin, AnonymousUserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app


class Follow(db.Model):
    __tablename__ = 'follows'
    follower_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                            primary_key=True)
    followed_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                            primary_key=True)

class Payment(db.Model):
    __tablename__ = 'payments'
    # common fields
    id = db.Column(db.Integer, primary_key=True)
    iframe_code = db.Column(db.String(200), index=True, nullable=False)
    # viewing fields
    # payer_viewed = db.Column(db.Boolean(), default=False) # has the payer viewed the receipt? -- this makes no sense
    payee_viewed = db.Column(db.Boolean(), default=False) # has the payee viewed the receipt?
    payer_emailed = db.Column(db.Boolean(), default=False) # have we emailed the receipt to the payer?
    payee_emailed = db.Column(db.Boolean(), default=False) # have we emailed the receipt to the payee?
    # coinbase callback fields
    transaction_time = db.Column(db.DateTime())
    payer_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    payee_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    amount_btc = db.Column(db.Integer) # stored in satoshi
    amount_native = db.Column(db.Integer) # stored in cents
    native_currency = db.Column(db.String(3)) # should move this to a separate table # - also can't actually be 64
    status = db.Column(db.String(12)) # should move to a separate table
    # message fields
    message_time = db.Column(db.DateTime())
    message_text = db.Column(db.String(200)) # must validate length before submission
    privacy = db.Column(db.Enum('PRIV', 'PUBL', 'ANON'))

    def add_transaction(self, transaction_time=transaction_time, payer_id=payer_id, payee_id=payee_id,
        amount_btc=amount_btc, amount_native=amount_native, native_currency=native_currency, status=status):
        if not self.transaction_time:
        # prevent overwrites
            self.transaction_time = transaction_time
            self.payer_id =  payer_id
            self.payee_id = payee_id
            self.amount_btc = amount_btc
            self.amount_native = amount_native
            self.native_currency = native_currency

    def add_message(self, message_time=message_time, message_text=message_text, privacy=privacy):
        if not self.message_time:
        # prevent overwrites
            self.message_time = message_time
            self.message_text = message_text
            self.privacy = privacy

    def viewed(self):
        if not self.payee_viewed:
            self.payee_viewed = True
            db.session.add(self)

    def sent_email_to_payee(self):
        if not self.payee_emailed:
            self.payee_emailed = True
            db.session.add(self)

    def sent_email_to_payer(self):
        if not self.payer_emailed:
            self.payer_emailed = True
            db.session.add(self)

    def amount_mbtc(self):
        return self.amount_btc / float(10000)

    def amount_native_formatted(self):
        return self.amount_native / float(100)

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    __searchable__ = ['username', 'name']  # these fields will be indexed by whoosh
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    confirmed = db.Column(db.Boolean, default=False)
    name = db.Column(db.String(64), index=True)

    first_name = db.Column(db.String(64), index=True)
    last_name = db.Column(db.String(64), index=True)

    payments_sent = db.relationship('Payment',
        foreign_keys=[Payment.payer_id],
        backref=db.backref('payer', lazy='joined'),
        lazy='dynamic')
    payments_received = db.relationship('Payment',
        foreign_keys=[Payment.payee_id],
        backref=db.backref('payee', lazy='joined'),
        lazy='dynamic')

    followed = db.relationship('Follow',
        foreign_keys=[Follow.follower_id],
        backref=db.backref('follower', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan')
    followers = db.relationship('Follow',
        foreign_keys=[Follow.followed_id],
        backref=db.backref('followed', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan')

    location = db.Column(db.String(64))
    about_me = db.Column(db.Text())
    member_since = db.Column(db.DateTime(), default=datetime.utcnow)
    last_seen = db.Column(db.DateTime(), default=datetime.utcnow)
    #twitter fields
    oauth_token = db.Column(db.String(200))
    oauth_secret = db.Column(db.String(200))
    twitter_id = db.Column(db.String(200))
    picture = db.Column(db.String(200))
    verified = db.Column(db.Boolean, default=False)
    #coinbase fields
    coinbase_oauth_token = db.Column(db.String(200))
    coinbase_oauth_refresh = db.Column(db.String(200))
    coinbase_oauth_expires = db.Column(db.DateTime(), default=datetime.utcnow)
    coinbase_code = db.Column(db.String(200), index=True) #need an index for message validity checking
    coinbase_email = db.Column(db.String(200), index=True)
    coinbase_name = db.Column(db.String(64), index=True)
    native_currency = db.Column(db.String(64))

    # contact preferences -- move these to a separate table later
    wants_email = db.Column(db.Boolean(), default=True)

    def unseen_payment_count(self):
        # payments_sent = self.payments_sent.filter_by(payer_viewed=False)
        payments_count = self.payments_received.filter_by(payee_viewed=False).count()
        # payments_count = payments_sent.union(payments_received).count()
        return payments_count

    def follow(self, user):
        if not self.is_following(user):
            f = Follow(follower=self, followed=user)
            db.session.add(f)

    def unfollow(self, user):
        f = self.followed.filter_by(followed_id=user.id).first()
        if f:
            db.session.delete(f)

    def is_following(self, user):
        return self.followed.filter_by(
            followed_id=user.id).first() is not None

    def is_followed_by(self, user):
        return self.followers.filter_by(
            follower_id=user.id).first() is not None

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)

    def delete_wallet(self):
        if self.coinbase_code:
            self.coinbase_oauth_token = None
            self.coinbase_oauth_refresh = None
            self.coinbase_oauth_expires = None
            self.coinbase_code = None

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    #Generates a token with a default validity time of one hour
    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    #checks that the ID from the token matches the logged in user, which is stored in 'current_user'
    #ensures that if a malicious user figures out how to generate signed tokens it cannot confirm
    #somone else's account.
    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        return True

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        return True


    def is_administrator(self):
        return self.can(Permission.ADMINISTER)

    def ping(self):
            self.last_seen = datetime.utcnow()
            db.session.add(self)

    def __repr__(self):
        return '<User %r>' % self.username


class AnonymousUser(AnonymousUserMixin):
    def can(self):
        return False

    def is_administrator(self):
        return False

login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))





