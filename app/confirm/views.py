from flask import render_template, redirect, request, url_for, flash, g
from flask.ext.login import login_user, logout_user, login_required, current_user
from . import confirm
from .. import db
from ..models import User
from .forms import ObtainEmailForm, SearchForm
from ..email import send_async_email, send_email
from app import oauth
#from config import MAX_SEARCH_RESULTS

#Logout must reside here, becuase users need to be able to logout
#while they are unconfirmed.



"""
@confirm.route('/unconfirmed', methods=['GET', 'POST'])
def unconfirmed():
    form = ObtainEmailForm()
    if current_user.is_anonymous() or current_user.confirmed:
        return redirect('main.index')
    if form.validate_on_submit():
        current_user.email = form.email.data
        db.session.commit()
        token = current_user.generate_confirmation_token()
        send_email(current_user.email, 'Confirm Your Account', 'confirm/email/confirm', user=current_user, token=token)
        flash('A confirmation email has been sent to you by email.')
        return redirect(url_for('confirm.unconfirmed'))
    return render_template('confirm/unconfirmed.html', form=form)

@confirm.route('/confirm/<token>')
@login_required
def confirm_email(token):
    if current_user.confirmed: #is the user already confirmed?
        return redirect(url_for('main.user', username=current_user.username))
    if current_user.confirm(token):
        flash('You have confirmed your account. Thanks!')
        return redirect(url_for('main.user', username=current_user.username))
    else:
        flash('The confirmation link is invalid or has expired.')
    return redirect(url_for('main.index'))

@confirm.route('/confirm-email')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_email('auth/email/confirm', 'Confirm Your Account', user, token=token)
    flash('A new confirmation email has been sent to you by email.')
    return redirect(url_for('main.index'))
"""
