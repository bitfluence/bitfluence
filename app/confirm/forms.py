from flask.ext.wtf import Form
from wtforms import TextField
from wtforms.validators import Required, Email, Regexp, EqualTo
from wtforms import ValidationError
from ..models import User


class ObtainEmailForm(Form):
    email = TextField('Email', validators=[Required(), Email()])

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')

class SearchForm(Form):
    search = TextField('search', validators = [Required()])