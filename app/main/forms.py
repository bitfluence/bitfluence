from flask.ext.wtf import Form
from wtforms import TextField, TextAreaField, BooleanField, SelectField,\
    SubmitField, RadioField, validators
from wtforms.validators import Required
#from wtforms import ValidationError
#from flask.ext.pagedown.fields import PageDownField
#from ..models import Role, User

class EditProfileForm(Form):
    name = TextField('Real name', [validators.Length(min=0, max=64)])
    location = TextField('Location', [validators.Length(min=0, max=64)])
    about_me = TextAreaField('About me', [validators.Length(min=0, max=300)])

class SearchForm(Form):
    search = TextField('search', validators = [Required()])

class FeedbackForm(Form):
	name = TextField('Name')
	email = TextField('Email')
	message = TextField('Message')

class MessageForm(Form):
	message = TextAreaField('Message', [validators.Length(min=0, max=100)])
	# privacy = RadioField('Privacy', choices=[('private','Pay privately'),('anon','Pay anonymously')])
	# privacy = RadioField('Privacy', choices=[('private','Pay privately'),('public','Pay publicly'),('anon','Pay anonymously')])
