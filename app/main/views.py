from flask import render_template, redirect, url_for, abort, flash, request, session, jsonify, json, g, make_response, current_app
import datetime
from dateutil.parser import *
from dateutil.tz import *
import hashlib
from . import main
from .. import models
from .. import db
from ..models import User, Payment
from flask.ext.login import login_required, current_user
from .forms import EditProfileForm, SearchForm, FeedbackForm, MessageForm
from app import oauth
from ..oauthconfig import coinbase
from ..email import send_feedback_email, send_wallet_email, send_received_money_email, send_sent_money_email
from Crypto.Cipher import AES
from flaskext.uploads import (UploadSet, configure_uploads, IMAGES,
                              UploadNotAllowed)
import uuid
from ..oauthconfig import twitter
import os
from ..photo_methods import resize_and_crop
import requests
from StringIO import StringIO
from PIL import Image

@main.before_app_request
def before_app_request():
    g.search_form = SearchForm()

@main.before_request
def before_request():
    if current_user.is_authenticated():
        current_user.ping()

@coinbase.tokengetter
def get_coinbase_oauth_token():
    return (get_oauth_token(current_user), '')

def get_oauth_token(user):
    if user.coinbase_oauth_expires is None:
        return None
    if user.coinbase_oauth_expires < datetime.datetime.utcnow():
        try:
            resp = coinbase.refresh('/oauth/token', refresh_token=user.coinbase_oauth_refresh)
            # this will fail if we can't connect to coinbase.com
            data = resp.data
            try:
                save_new_oauth_token(user, data) # This will fail if the access was denied
                # we should probably clear out the refresh token if it failed
            except:
                return None
        except:
            return None
    return user.coinbase_oauth_token

def save_new_oauth_token(user, data):
    user.coinbase_oauth_expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=data['expires_in'])
    user.coinbase_oauth_token = data['access_token']
    user.coinbase_oauth_refresh = data['refresh_token']
    db.session.commit()
    return

def new_iframe(payee, payer=None, price=5.00):
    # probably should move many of these arguments into **kwargs
    # optional arguments: really everything except for payee

    # TODO: find a cleaner way to access configs
    app = current_app._get_current_object()
    encrypter = AES.new(app.config['AES_KEY'])

    if (payer is None) or (not payer.is_authenticated()):
        currency = payee.native_currency
        message = '{"payer":null,"payee":%i}' % payee.id
    else:
        currency = payer.native_currency
        message = '{"payer":%i,"payee":%i}' % (payer.id, payee.id)
    message = message.zfill(32)
    # this will be too small when (if) ids get in the millions
    ciphertext = encrypter.encrypt(message)
    url_secret = ciphertext.encode("hex")

    description = "Sent money to @%s via @Bitfluence" % payee.username
    callback_url = "%spayment_callback?secret=%s" % (app.config['URL'], url_secret) if app.config['URL'] is not None else None

    datetime_str = "%s" % datetime.datetime.utcnow()
    request_data = {
                    "button": {
                        "custom": hashlib.md5(datetime_str).hexdigest(), # added to guarantee every custom iframe code is unique
                        "custom_secure": True, # not quite sure what this does, but it sounds good
                        "name": "Bitfluence",
                        "type": "buy_now",
                        "description": description,
                        "price_string": price,
                        "callback_url": callback_url, # need to figure out what to do with non-custom callbacks
                        "price_currency_iso": currency, #unless we are creating a custom iframe
                        "variable_price": True,
                        "choose_price": True,
                        "style": "none"
                    }
                }
    # maybe only send token if payee != current_user
    token = (get_oauth_token(payee), '') # if this fails, the coinbase.request token will not exist
    if token != (None, ''):
        coinbase_iframe_resp = coinbase.request('/api/v1/buttons', token=token, data=json.dumps(request_data), method='POST', content_type='application/json')
        if coinbase_iframe_resp.status == 200:
            coinbase_code = coinbase_iframe_resp.data['button']['code']
            return coinbase_code
    return None

def example_mesage():
    messages = [
    "Yeah....sorry I pirated all your music back in '03....here's a buck",
    "I'll send you a bit if you send one back. ;)",
    "Hey friend, here's a bit....er I mean a micro...eh I mean a milli...a millibit....something like that",
    "Quick, hide this money before the NSA finds it!",
    "Thanks for being my friend. Here's a dollar of magic internet money.",
    "Dude! Thanks again for buying the pizza!",
    "Yesterday you were...amazing...can't believe that's illegal in 38 states!",
    "I loved your webcomic from last week! That really cheered my day. :)",
    "Hey, thanks for being so cool and giving up sleep to make this great website for us to use. :)",
    "My hard drive was getting too heavy, so here's a BTC for you!",
    "I really appreciate your videos -- I wouldn't have graduated college without them!",
    "Here's the money for the gas bill...so high....we should totally turn the heat down!",
    "Happy Monday. Have some decentralized universal digital currency on me!"
    ]
    import random;
    return random.choice(messages)




# # ************************************************************************************************

# START ROUTING HERE -- HELPER FUNCTIONS ABOVE

# # ************************************************************************************************



@main.route('/searchusers', methods = ['POST'])
def searchusers():
    if g.search_form.validate_on_submit():
        return redirect(url_for('main.search_results', query=g.search_form.search.data))
    return redirect(url_for('main.index'))

@main.route('/search_results/<query>')
def search_results(query):
    results = User.query.whoosh_search(query, or_=True)
    return render_template('search_results.html', query = query, results = results)

@main.route('/search', methods = ['POST'])
def search():
    if not g.search_form.validate_on_submit():
        return redirect(url_for('main.index'))
    return redirect(url_for('search_results', query = g.search_form.search.data))

@main.route('/login-coinbase')
# @login_required
def login_coinbase():
    if not current_user.is_authenticated():
        return redirect(url_for('auth.login_twitter'))
    #If no coinbase tokens are present
    if current_user.coinbase_oauth_token is None:
        #proceed to log in.
        callback_url = url_for('main.coinbase_authorized', _external=True)
        return coinbase.authorize(callback=callback_url)
    #User can not log in unless they delete a wallet
    else:
        return redirect(url_for('main.index'))

@main.route('/login-coinbase-authorized')
# @login_required
@coinbase.authorized_handler
#Only those who are logging in for the first time will be able
#to access this view.
def coinbase_authorized(resp):
    if not current_user.is_authenticated():
        return redirect(url_for('auth.login_twitter'))
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args.get('error_reason'),
            request.args.get('error_description')
        )
    save_new_oauth_token(current_user, data=resp)

    user_data_resp = coinbase.request('/api/v1/users', method='GET', content_type='application/json')
    if user_data_resp.status == 200:
        current_user.coinbase_email = user_data_resp.data['users'][0]['user']['email']
        current_user.native_currency = user_data_resp.data['users'][0]['user']['native_currency']
        current_user.coinbase_name = user_data_resp.data['users'][0]['user']['name']
        db.session.commit()
    else:
        return "There was an error pulling your coinbase user information."

    # need to catch an error here
    current_user.coinbase_code = new_iframe(payee=current_user)
    db.session.commit()
    send_wallet_email(current_user.coinbase_email,
                          'Coinbase Wallet Connected',
                          'notify/email/new_wallet',
                          user=current_user)
    flash("That's it! Your profile is complete.")
    return redirect(url_for('.user', username=current_user.username))


@main.route('/')
def index():
    if current_user.is_authenticated():
        return redirect(url_for('.user', username=current_user.username))
    form = FeedbackForm()
    return render_template('index.html', form=form)

@main.route('/message', methods=['POST'])
def receive_message():
    form = MessageForm()
    if form.validate_on_submit():
        message_text = form.message.data
        # privacy_raw = form.privacy.data
    else:
        return make_response(jsonify( { 'error': 'Form did not validate' } ), 400)

    # if privacy_raw == 'private':
    #     privacy = 'PRIV'
    # elif privacy_raw == 'public':
    #     privacy = 'PUBL'
    # elif privacy_raw == 'anon':
    #     privacy = 'ANON'
    # else:
    #     return error


    iframe_code = request.args.get("iframe_code")
    user_count = User.query.filter_by(coinbase_code=iframe_code).count()
    if user_count > 0:
        # iframe_code is a default one, meaning we can't guarantee uniqueness and hence can't attach a message
        return make_response(jsonify( { 'error': 'Did not accept post -- default iframe_code detected' } ), 200)

    message_details = dict(
        message_text = message_text,
        message_time = datetime.datetime.utcnow(),
        privacy = 'PRIV'
        )

    payment = Payment.query.filter_by(iframe_code=iframe_code).first()
    payment_count = Payment.query.filter_by(iframe_code=iframe_code).count()
    if payment_count > 1:
        # since we are checking for default iframe_code above, this should never happen
        # TODO: log this somewhere so we know if it ever happens in the wild
        return make_response(jsonify( { 'error': 'Did not accept post -- more than one linked payment detected' } ), 200)
        # 200 response means user will think this was saved....

    if payment is None:
        payment = Payment(iframe_code = iframe_code, **message_details)
    else:
        payment.add_message(**message_details)
        if payment.payee_emailed is False and payment.payee != None:
            send_received_money_email(payment)
            payment.sent_email_to_payee()
        if payment.payer_emailed is False and payment.payer != None:
            send_sent_money_email(payment)
            payment.sent_email_to_payer()

    db.session.add(payment)
    db.session.commit()
    return make_response(jsonify(message_details), 200)

@main.route('/payments')
def payments():
    if not current_user.is_authenticated():
        return redirect(url_for('auth.login_twitter'))
    # we do not want to return orphan messages here. Since the message is linked to the user through the payemnt callback,
    # no orphan messages will be returned
    payments_sent = current_user.payments_sent
    payments_received = current_user.payments_received
    payments = payments_sent.union(payments_received).order_by(Payment.transaction_time.desc()).all()
    for payment in payments:
        # was the current_user the receiver?
        if payment.payee_id == current_user.id:
            payment.sent = False
            payment.viewed()
        else:
            payment.sent = True
        # payment.currency_symbol = current
    db.session.commit()
    template = 'payments_table.html' if request.args.get("style") == 'table' else 'payments_messages.html'
    return render_template(template, payments=payments)

@main.route('/payment_callback', methods=['POST'])
def receive_callback():
    app = current_app._get_current_object()
    encrypter = AES.new(app.config['AES_KEY'])

    try:
        secret = request.args.get("secret").decode("hex")
    except:
        return make_response(jsonify( { 'error': 'Did not accept post' } ), 200)
    message = encrypter.decrypt(secret)
    # if the message can't be decrypted, this will fail:
    if not (message[0] == '0' and message[-1] == '}'):
        # notify coinbase that we recieved the callback, but we will not accept it internally
        return make_response(jsonify( { 'error': 'Did not accept post' } ), 200)

    message_json = json.loads(message.lstrip("0"))
    coinbase_json = request.get_json().get("order")
    iframe_code = coinbase_json["button"]["id"]
    payee_id = message_json["payee"]
    transaction_time = parse(coinbase_json["created_at"]).astimezone(tzutc()).strftime("%Y-%m-%d %H:%M:%S")

    payment_details = dict(
        payer_id = message_json["payer"], # can be null or empty string
        payee_id = message_json["payee"], # should never be null
        transaction_time = transaction_time,
        amount_btc = coinbase_json["total_btc"]["cents"],
        amount_native = coinbase_json["total_native"]["cents"],
        native_currency = coinbase_json["total_native"]["currency_iso"],
        status = coinbase_json["status"]
        )

    payment = Payment.query.filter_by(iframe_code=iframe_code).first()
    payment_count = Payment.query.filter_by(iframe_code=iframe_code).count()

    if (payment is not None):
        if payment_count > 1:
            # this should never occur
            return make_response(jsonify( { 'error': 'Did not accept post' } ), 200)
        # unique iframe payment where message was received first
        # only time an update is required (versus insert)
        payment.add_transaction(**payment_details)
        if payment.payee_emailed is False and payment.payee != None:
            send_received_money_email(payment)
            payment.sent_email_to_payee()
        if payment.payer_emailed is False and payment.payer != None:
            send_sent_money_email(payment)
            payment.sent_email_to_payer()
    else:
        # still save payments from non-default iframe_codes here -- just can't have messages
        payment = Payment(iframe_code=iframe_code, **payment_details)
    db.session.add(payment)
    db.session.commit()
    return make_response(jsonify( { 'success': 'Received callback post' } ), 200)

uploaded_photos = UploadSet('photos', IMAGES)

@main.route('/edit-profile', methods=['GET', 'POST'])
# @login_required
def edit_profile():
    if not current_user.is_authenticated():
        return redirect(url_for('auth.login_twitter'))
    if current_user.coinbase_code:
        email = current_user.coinbase_email
        name = current_user.coinbase_name
    else:
        email = ""
        name = ""

    form = EditProfileForm()
    if form.validate_on_submit():
        photo = request.files.get('photo')
        if (photo):
            try:
                # save the image
                filename = uploaded_photos.save(photo, name=hex(uuid.uuid4().time)[2:-1]+'.')
                # crop the image
                app_dir = os.path.dirname(os.path.abspath(models.__file__))
                photos_dir = os.path.join(app_dir, "static/uploads/photos/")
                profile_img_location = photos_dir + filename
                cropped_filename = 'cropped_' + filename
                cropped_profile_img_location = photos_dir + cropped_filename
                resize_and_crop(profile_img_location, cropped_profile_img_location, (200,200), crop_type='middle')
                # get full path to existing pictures (original and cropped)
                existing_picture = app_dir + current_user.picture
                existing_picture_orig = existing_picture.replace("cropped_","")
                # save to new (uploaded) picture path in database
                current_user.picture = '/static/uploads/photos/' + cropped_filename
                # try to delete the old image from the uploads folder
                if os.path.isfile(existing_picture):
                    os.remove(existing_picture)
                    os.remove(existing_picture_orig)

            except UploadNotAllowed:
                flash("There was an error uploading your picture.")
            #else:
                #flash("You've successfully uploaded a new picture!")
        current_user.name = form.name.data
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data
        db.session.add(current_user)
        flash('Your profile has been updated!')
        return redirect(url_for('.user', username=current_user.username))

    form.name.data = current_user.name
    form.location.data = current_user.location
    form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', current_user=current_user, form=form, name=name, email=email)

@main.route('/delete-wallet')
# @login_required
def delete_wallet():
    if current_user.coinbase_code:
        current_user.delete_wallet()
        flash('Wallet deleted')
    else:
        flash('You do not have a registered coinbase wallet to delete.')
    return redirect(url_for('main.edit_profile'))


@main.route('/<username>')
def user(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        abort(404)
    form = MessageForm()
    if user.coinbase_code is None:
        iframe_code = None
    elif user.coinbase_oauth_refresh is None:
        iframe_code = user.coinbase_code
    else:
        iframe_code = new_iframe(payee=user, payer=current_user)
        if iframe_code is None:
            iframe_code = user.coinbase_code

    message = example_mesage()

    return render_template('user.html', form=form, user=user, iframe_code=iframe_code, message=message)

@main.route('/contacts')
# @login_required
def contacts():
    if not current_user.is_authenticated():
        return redirect(url_for('auth.login_twitter'))
    follows = [item.followed for item in current_user.followed]
    return render_template('contacts.html', follows=follows)

@main.route('/add/<username>')
# @login_required
def follow(username):
    if not current_user.is_authenticated():
        return redirect(url_for('auth.login_twitter'))
    user = User.query.filter_by(username=username).first()
    if user is None:
        return redirect(url_for('main.index'))
    if current_user.is_following(user):
        #Should pass in a variable to display message that user is already following at some future point.
        return redirect(url_for('.user', username=username))
    current_user.follow(user)
    flash("Added to contact list: %s" % username)
    return redirect(url_for('.user', username=username))

@main.route('/remove/<username>')
# @login_required
def unfollow(username):
    if not current_user.is_authenticated():
        return redirect(url_for('auth.login_twitter'))
    user = User.query.filter_by(username=username).first()
    if user is None:
        return redirect(url_for('main.index'))
    if not current_user.is_following(user):
        return redirect(url_for('.user', username=username))
    current_user.unfollow(user)
    flash("Removed from contact list: %s" % username)
    return redirect(url_for('.user', username=username))

@main.route('/feedback', methods=['POST'])
def feedback():
    form = FeedbackForm()
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        message = form.message.data
        send_feedback_email(name, email, message)
    return redirect(url_for('.index'))

@main.route('/help/faq')
def faq():
    return render_template('faq.html')

@main.route('/contact_us')
def contact_us():
    form = FeedbackForm()
    return render_template('contact_us.html', form=form)

@main.route('/company/about')
def about():
    username = 'thair1'
    team = User.query.filter(User.username.in_(['thair1', 'evbots']))
    return render_template('about.html', team=team)

@main.route('/company/join')
def join():
    return render_template('join.html')

@main.route('/company/announcement')
def announcement():
    return render_template('announcement.html')


"""
@main.route('/new', methods=['GET', 'POST'])
def new():
    if request.method == 'POST':
        photo = request.files.get('photo')
        if not (photo):
            flash("You must select a file for upload")
        else:
            try:
                filename = uploaded_photos.save(photo, name=hex(uuid.uuid4().time)[2:-1]+'.')
            except UploadNotAllowed:
                flash("There was an error uploading your picture.")
            else:
                flash("You've successfully uploaded a new picture!")

    return render_template('photo.html')
"""



"""
@main.route('/remembered-by/<username>')
def followers(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        return redirect(url_for('.index'))
    #page = request.args.get('page', 1, type=int)
    #pagination = user.followers.paginate(
        #page, per_page=current_app.config['FLASKY_FOLLOWERS_PER_PAGE'],
        #error_out=False)
    follows = [{'user': item.follower, 'timestamp': item.timestamp}
               for item in pagination.items]
    return render_template('remembered_by.html', user=user, follows=follows)


@main.route('/remembering/<username>')
def followed_by(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    pagination = user.followed.paginate(
        page, per_page=current_app.config['FLASKY_FOLLOWERS_PER_PAGE'],
        error_out=False)
    follows = [{'user': item.followed, 'timestamp': item.timestamp}
               for item in pagination.items]
    return render_template('followers.html', user=user, title="Followed by",
                           endpoint='.followed_by', pagination=pagination,
                           follows=follows)
"""
