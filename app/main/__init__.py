"""
Blueprints are a way to define routes in the global scope after
the creation of the application is moved into a factory function.
"""
from flask import Blueprint

main = Blueprint('main', __name__)
#all routes are now associated with the 'main' blueprint. example: @main.index('/')

from . import views, errors
