from threading import Thread
from flask import current_app, render_template
from flask.ext.mail import Message
from . import mail
import random

def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

def send_email(to, subject, template, **kwargs):
    app = current_app._get_current_object()
    msg = Message(app.config['APP_MAIL_SUBJECT_PREFIX'] + ' ' + subject, sender=app.config['APP_MAIL_SENDER'], recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    #msg.html = render_template(template + '.html', **kwargs)
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

def send_wallet_email(to, subject, template, **kwargs):
    app = current_app._get_current_object()
    msg = Message(subject, sender='Bitfluence <admin@bitfluence.com>', recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

def send_feedback_email(name, email, message):
    app = current_app._get_current_object()
    subject = "Feedback from " + name
    msg = Message(app.config['APP_MAIL_SUBJECT_PREFIX'] + ' ' + subject, sender=app.config['APP_MAIL_SENDER'], recipients=["twh116@gmail.com"], cc=["botellde@gmail.com"])
    msg.body = "Name: " + name + "\nEmail: " + email + "\n\nMessage: " + message
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

def send_received_money_email(payment):
    kwargs = dict(
        goodbye = example_goodbye(),
        banner_image = random_banner_image(),
        user = payment.payee,
        payment = payment
        )
    template = 'notify/email/receive_payment'
    subject = 'Payment Received'
    to = payment.payee.coinbase_email
    app = current_app._get_current_object()
    msg = Message(subject, sender='Bitfluence <admin@bitfluence.com>', recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

def send_sent_money_email(payment):
    kwargs = dict(
        goodbye = example_goodbye(),
        banner_image = random_banner_image(),
        user = payment.payer,
        payment = payment
        )
    template = 'notify/email/send_payment'
    subject = 'Payment Sent'
    to = payment.payer.coinbase_email
    app = current_app._get_current_object()
    msg = Message(subject, sender='Bitfluence <admin@bitfluence.com>', recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

def send_earlybird_error_email(user):
    kwargs = dict(
        banner_image = random_banner_image(),
        user = user
        )
    template = 'notify/email/earlybird_error'
    subject = 'Bitfluence Update'
    to = user.coinbase_email
    app = current_app._get_current_object()
    msg = Message(subject, sender='Bitfluence <admin@bitfluence.com>', recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

# ---------------------- Helper functions ----------------------
def example_goodbye():
    goodbyes = [
    "Cheers,",
    "Thanks,",
    "Keep being awesome,",
    "Yours truly,",
    "Til next time,",
    "Your faithful servant,",
    "In crypto we trust,",
    "For House Stark,",
    "Your friends,"
    ]
    return random.choice(goodbyes)

def random_banner_image():
    images = [
    'bigimage_whale1.jpg',
    'bigimage_zion1.jpg',
    'bigimage_yemen1.jpg',
    'bigimage_swamp1.jpg',
    'bigimage_spain1.jpg',
    'bigimage_snow1.jpg',
    'bigimage_russia1.jpg',
    'bigimage_rainforest1.jpg',
    'bigimage_poland1.jpg',
    'bigimage_peru1.jpg',
    'bigimage_outback1.jpg',
    'bigimage_meteors1.jpg',
    'bigimage_iraq1.jpg',
    'bigimage_hongkong1.jpg',
    'bigimage_frog1.jpg',
    'bigimage_flight1.jpg',
    'bigimage_earth1.jpg',
    'bigimage_desert1.jpg',
    'bigimage_coralreef2.jpg',
    'bigimage_artic1.jpg',
    'bigimage_arch1.jpg',
    'bigimage.jpg'
    ]
    return random.choice(images)

