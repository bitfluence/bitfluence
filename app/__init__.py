from flask import Flask
from flask.ext.mail import Mail
from flask.ext.moment import Moment
from flask.ext.sqlalchemy import SQLAlchemy
from config import config
from flask.ext.login import LoginManager
#from flask_oauth import OAuth
from flask_oauthlib.client import OAuth
import flask.ext.whooshalchemy

from flaskext.uploads import (UploadSet, configure_uploads, IMAGES,
                              UploadNotAllowed, patch_request_class)


"""
This constructor imports most of the Flask extensions currently in use and
creates them uninitialized by passing no arguments into their constructors.
"""

mail = Mail()
moment = Moment()
db = SQLAlchemy()
oauth = OAuth()

login_manager = LoginManager()
#login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

"""
This application factory gives the script time to set the configuration
and the ability to create multiple application instances.
"""

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.config['WHOOSH_BASE'] = 'search'
    app.config['MANDRILL_API_KEY'] = 'K53zBF_iVBg6izaIyW6eqg'
    config[config_name].init_app(app)
    #once the app is created and configured the extensions can be initialized.
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    oauth.init_app(app)



    # For photo uploads
    from main.views import uploaded_photos
    configure_uploads(app, uploaded_photos)
    # Max upload set to 3 megabytes
    patch_request_class(app, size=3145728)

    #means must use @main.route('/') for views in the 'main' folder
    from main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    #means must use @auth.route('/') for views in the 'auth' folder
    from auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)
    #app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from confirm import confirm as confirm_blueprint
    app.register_blueprint(confirm_blueprint)

    return app
