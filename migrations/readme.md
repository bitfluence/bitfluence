Creating new database
=========

To create a new database:

	create user 'bitfluence'@'localhost' identified by PASSWORD '*F4206CC45F5FFD8411A774421550F5018530B7F9';

    create database bitfluence;
    grant all privileges on bitfluence.* to 'bitfluence'@'localhost';


Running migrations after git pull
========

To update a database after a git pull: (this will also serves as init if you have an empty database)

	python manage.py db upgrade

New migrations
======

To create a migration script using automatic migrations (warning: can be innacurate), simply update models.py and then run:

	python manage.py db migrate -m "<migration name>"

Then, to run the migration script (this actually changes mysql database):

	python manage.py db upgrade

Notes
=====

There is no need to run `db init` because the migrations folder has already been created and checked into git
