"""add verified status to model and fix spelling error

Revision ID: e322e8a2550
Revises: 338bfbc2edba
Create Date: 2014-03-15 10:30:44.088481

"""

# revision identifiers, used by Alembic.
revision = 'e322e8a2550'
down_revision = '338bfbc2edba'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('native_currency', sa.String(length=64), nullable=True))
    op.add_column('users', sa.Column('verified', sa.Boolean(), nullable=True))
    op.drop_column('users', 'native_curreny')
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('native_curreny', mysql.VARCHAR(length=64), nullable=True))
    op.drop_column('users', 'verified')
    op.drop_column('users', 'native_currency')
    ### end Alembic commands ###
